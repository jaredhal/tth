This is a really basic table top helper for DM's hoping to speed up the
character creation process for themselves and their players.
I've tried to make it extendable and customizable, and most importantly, easy
to get running with little-to-no knowledge. There are a lot of mistakes and
not-best-practices, but it's easy to use and a work in progress.

P.S. this should be fine to run locally, but don't expose it to the internet
unless you know what you're doing. This definitely has a lot of security flaws.

**Instructions to set up**

**WINDOWS**

Install [Python](https://www.python.org/downloads/). Be sure to check the 'add to PATH' box at the end!

[Download](https://gitlab.com/jaredhal/tth/-/archive/master/tth-master.zip) the TTH repository and unzip it

Move into the folder you just unzipped.

For this, and the following instructions, you'll want to enter them into a command prompt.
  
Create a virtual environment with python3.
  
`python -m virtualenv -p python3 venv`

Activate the environment
  
`venv\Scripts\activate.bat`

Install the dependencies
  
`python -m pip install -r requirements.txt`
 
set the name of the app for Flask
 
`set FLASK_APP=tth`

create the database, you only need to do this the first time
   
`flask init-db`

start the server
 
`waitress-serve --call tth:create_app`


**LINUX** 

Download or clone the TTH repository

`git clone https://gitlab.com/jaredhal/tth.git`

Move into the 'tth' directory, then create a virtual environment with python3. 
I prefer virtualenv, and call mine 'venv' but you can use whatever you like.

`virtualenv -p python3 venv`

activate it

`source venv/bin/activate`

install the dependencies

`python -m pip install -r requirements.txt`

export the name of the app for Flask

`export FLASK_APP=tth`

Initialize the database. You only need to do this the first time

`flask init-db`

Start up the server!

`waitress-serve --call 'tth:create_app'`


   
 

