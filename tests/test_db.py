import sqlite3, pytest

def test_init_db_command(runner, monkeypatch):

    result = runner.invoke(args=['init-db'])
    assert 'database initialized' in result.output
