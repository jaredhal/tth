import pytest

from flask import session

from tth.models import User

def test_urls(client, app, auth):
    assert client.get('/register').status_code == 200
    assert client.get('/login').status_code == 200
    auth.login()
    response = client.get('/logout', follow_redirects=True)
    assert b'logged out' in response.data

def test_register_form(client, app):
    with app.app_context():
        from tth.forms import Register_Form
        form = Register_Form(
            username='userX'
            ,password='password123'
            ,confirm_password='password123'
            ,meta={'csrf':False}
        )
        assert form.validate()

def test_login_form(client, app):
    with app.app_context():
        from tth.forms import Login_Form
        form = Login_Form(
            username='userX'
            ,password='password123'
            ,meta={'csrf':False}
        )
        assert form.validate()

def test_register(client, app, monkeypatch):

    def validate_spoof(self):
        return True

    monkeypatch.setattr(
        'tth.forms.Register_Form.validate_on_submit'
        ,validate_spoof
    )
    with app.app_context():
        data = {
            'username':'userX'
            ,'password':'password123'
            ,'confirm_password':'password123'
        }
        response = client.post('/register', data=data, follow_redirects=False)
        assert response.status_code == 302
        assert User.query.filter_by(username='userX').first() is not None


def test_login(client, app, monkeypatch):
    with app.app_context():
        data = {
            'username':'test2'
            ,'password':'testing'
            ,'confirm_password':'testing'
        }
        client.post('/register', data=data)
        assert User.query.filter_by(username='test2').first() is not None

        response = client.post('/login', data=data, follow_redirects=False)
        assert response.status_code == 302
        assert 'http://localhost/' in response.headers['Location']
        with client:
            client.get('/')
            # conftest auth object creates user with id 1
            assert session['id'] == 2

def test_logout(client, app, auth):
    auth.login()
    assert b'logout' in client.get('/', follow_redirects=True).data
    response = client.get('/logout', follow_redirects=True)
    assert response.status_code == 200
    assert b'login' in response.data
