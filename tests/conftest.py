import os, tempfile, pytest

from tth import create_app
from tth.db import db
from tth.models import *
from werkzeug.security import generate_password_hash

@pytest.fixture
def app():
    """Create new app instance for each test"""
    db_fd, db_path = tempfile.mkstemp()
    app = create_app({
        'TESTING': True
        ,'WTF_CSRF_ENABLED': False
        ,'SECRET_KEY': 'testing'
        ,'SQLALCHEMY_DATABASE_URI':'sqlite:///:memory:'
        ,'SQLALCHEMY_TRACK_MODIFICATIONS':False
    })
    with app.app_context():
        db.init_app(app)
        db.drop_all()
        db.create_all()
        db.session.add(
            User(username='test', password=generate_password_hash('testing'))
        )
        db.session.commit()
    yield app

    os.close(db_fd)
    os.unlink(db_path)

@pytest.fixture
def client(app):
    """ test client """
    return app.test_client()

@pytest.fixture
def runner(app):
    return app.test_cli_runner()

class AuthActions(object):
    def __init__(self, client):
        self._client = client

    def login(self, username='test',password='testing'):
        response = self._client.post('/login'
            ,data={'username':username, 'password': password}
        )
        print('LOGIN RESPONSE', response)
        print(vars(response))
        return response

    def logout(self):
        return self._client.post('/logout')

@pytest.fixture
def auth(client):
    return AuthActions(client)
