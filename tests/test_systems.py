import pytest

@pytest.mark.parametrize('path', (
    'systems/list'
    ,'systems/create'
))
def test_login_required(client, path):
    response = client.get(path)
    assert response.status_code == 302
    assert response.headers['Location'] == 'http://localhost/login'

def test_system_form(client, app, monkeypatch):
    with app.app_context():
        from tth.forms import System_Form, Node_Form
        sys_form = System_Form(
            sys_title='test_title'
            ,node_list=[('test1','test1'),('test2','test2')]
        )
        assert sys_form.validate()

        from tth.nodes import get_nodes
        n_form = Node_Form(
            node_title='test title'
            ,node_type=get_nodes()[0]
            ,node_config='{}'
        )

        assert n_form.validate()
        n_form = Node_Form(
            node_title='test title'
            ,node_type=''
            ,node_config = '{}'
        )
        assert n_form.validate() is False

def test_system_create(client, auth, app):
    auth.login()
    assert b'logout' in client.get('/', follow_redirects=True).data
