import os

from flask import current_app

def get_nodes():
    return os.listdir(os.path.join(current_app.root_path,'static/nodes'))
