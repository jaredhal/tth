$('input.confirmAsk').click( e => {
	let i = $('input.confirmAsk').index(e.currentTarget);
	let check = $($('input.confirmAsk').get(i));
	let conf = $($('input.confirmConfirm').get(i));
	let canc = $($('input.confirmCancel').get(i));
	check.hide();
	conf.show();
	canc.show();
});

$('input.confirmCancel').click( e => {
	let i = $('input.confirmCancel').index(e.currentTarget);
	let check = $($('input.confirmAsk').get(i));
	let conf = $($('input.confirmConfirm').get(i));
	let canc = $($('input.confirmCancel').get(i));
	check.show();
	conf.hide();
	canc.hide();
});

$('input.confirmConfirm').click( e => {
	let i = $('input.confirmConfirm').index(e.currentTarget);
	let check = $($('input.confirmAsk').get(i));
	let conf = $($('input.confirmConfirm').get(i));
	let canc = $($('input.confirmCancel').get(i));
	check.show();
	conf.hide();
	canc.hide();
});
