//global consts, nodes and tth defined in template
// nodes is array of node names as string
// tth is { '${node name} : {init: initializing func, template: template literal }}

function i(id) {
	return $('#' + id);
}

const sysForm = (function () {
	let title, nodeList; //values
	let titleInput, displayList, nodeInput, removeInput; //elements

	function addNode(title, type, config){
		displayList.append(`<li>${title}</li>`);
		nodeList.push({
			'title': title
			,'type': type
			,'config': config
		});
		nodeInput.val(JSON.stringify(nodeList));
	}

	function removeNode(){
		if (displayList.children().length > 0 && nodeList.length > 0){
			displayList.children()[displayList.children().length - 1].remove();
			nodeList.pop();
		}
	}

	function resetForm() {
		title = null;
		nodeList = [];
		titleInput.val(null);
		nodeInput.val(null);
	}

	function connectForm(titleId, hiddenId, submitId, displayId, removeId) {
		title = null;
		nodeList = [];
		titleInput = i(titleId);
		displayList = i(displayId);
		nodeInput = i(hiddenId);
		removeInput = i(removeId);
		removeInput.click(removeNode);
}

	return ({
		addNode: addNode
		,connectForm: connectForm
		,resetForm: resetForm
	});

})();

const nodeForm = (function () {
	let title, type, config; //values
	let titleInput, typeRadio, container, submitBtn; //elements
	let submitCallback = function() {};

	const defaultContentValidator = function(){
		return true;
	};

	let contentValidator = defaultContentValidator;

	function changeType(e){
		console.log('changing type');
		type = e.target.value;
		container.html(null);
		config = null;
		setContentValidator(defaultContentValidator);
		//tth is global object defined in template
		container.append(tth[type].template);
		if ("init" in tth[type] ) {
			tth[type].init();
		}
		checkValid();
	}

	function setTitle(e) {
		title = e.target.value;
		checkValid();
	}

	function setConfig(value) {
		config = value;
		console.log('system_maker.js:setConfig(value) - config is ', config);
		checkValid();
	}

	function setCallback(func) {
		submitCallback = func;
	}

	function setContentValidator(func) {
		console.log("content validator has been set to", func);
		contentValidator = func;
	}

	function submitNode() {
		if(checkValid()){
			submitCallback(title, type, config);
			resetForm();
		}
	}

	function resetForm() {
		title = null, type = null, config = null;
		titleInput.val(null);
		container.html(null);
		node_config.value = null;
		typeRadio.prop('checked', false);
		submitBtn.prop('disabled', true);
		contentValidator = defaultContentValidator;
	}

	function checkValid() {
		let valid = false;
		if (titleInput.val() && type && contentValidator() ){
			valid = true;
		}
		submitBtn.prop('disabled', !(valid));
		return valid;
	}

	function connectForm(titleId, radioName, containerId, submitId) {
		titleInput = i(titleId);
		container = i(containerId);
		submitBtn = i(submitId);
		typeRadio = $('input[name="' + radioName + '"]');

		titleInput.change(setTitle);
		typeRadio.change(changeType);
		submitBtn.click(submitNode);
	}

	return ({
		setConfig: setConfig
		,connectForm: connectForm
		,resetForm: resetForm
		,setCallback: setCallback
		,setContentValidator: setContentValidator
	});
})();

$(document).ready(function(){
	sysForm.connectForm('sys_title','node_list','sys_submit','nodeDisplay','removeNode');
	sysForm.resetForm();
	nodeForm.connectForm('node_title', 'node_type', 'nodeContainer', 'node_submit');
	nodeForm.resetForm();
	nodeForm.setCallback(sysForm.addNode);
	tth['setNodeConfig'] = nodeForm.setConfig;
	tth['setContentValidator'] = nodeForm.setContentValidator;
});
