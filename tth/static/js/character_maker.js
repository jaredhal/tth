//global 'tth' defined in template
/* tth is a dictionary of { 'node type' : {
	,template: template literal
	,init: initializing function which takes the config JSON blob generated by system.js
	}}
*/

const charForm = (function(){
	let nodes, nodeIndex, traits;
	let nodeContainer, nodeUL, traitsInput, submitBtn;

	function joinTraits(newTraits, oldTraits) {
		Object.keys(newTraits).forEach( (key) => {
			if ( !(key in oldTraits)) {
				oldTraits[key] = newTraits[key];
			} else {
				oldTraits[key] += newTraits[key];
			}
		});
		return oldTraits;
	}

	function loadNewNode(index) {
		let n = nodes[index];
		nodeContainer.html(tth[n.type]['template']);
		if ('init' in tth[n.type]){
			tth[n.type].init(n.title, n.config, nodeCallback);
		}
	}

	function nodeCallback(newTraits, displayedTrait=null) {
		traits = joinTraits(newTraits, traits);
		traitsInput.val(JSON.stringify(traits));

		console.log("Submitted new traits:", newTraits);
		let display = '' + nodes[nodeIndex].title + '';
		if (displayedTrait != null) {
			nodeUL.append($('<li/>').html(display+':'+displayedTrait));
		} else {
			nodeUL.append($('<li/>').html(display));
		}
		nodeIndex += 1;
		if (nodeIndex >= nodes.length){
			nodeContainer.html('your character is done!');
			submitBtn.prop('disabled', false);
		} else {
			loadNewNode(nodeIndex);
		}
		console.log("Character traits are now:", traits);
	}

	function connectForm(containerId, nodeULId, traitsId, btnId, nodeList) {
		nodeContainer = $('#'+containerId);
		nodeUL = $('#' + nodeULId);
		traitsInput = $('#'+traitsId);
		submitBtn = $('#'+btnId);
		nodes = nodeList;
		traits = new Object();
		if (nodes.length == 0){
			console.log('no nodes');
			submitBtn.prop('disabled', false);
			nodeContainer.html('done!');
		} else {
			console.log('nodes exist');
			nodeIndex = 0;
			submitBtn.prop('disabled', true);
			loadNewNode(nodeIndex);
		}
	}

	return ({
		connectForm: connectForm
	});
}());

$(document).ready(function(){
	charForm.connectForm('nodeContainer', 'nodeList' ,'traits', 'submit', nodes);
});
