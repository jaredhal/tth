You can make your own nodes.

Make a folder inside the nodes folder with the name of your node.
Inside the folder, create two files: character.js, system.js.
The scripts in the js folder, system_maker.js and character_maker.js, will interact with your node through the global tth.

# system.js

On the system side, tth[] is a global that contains 2 functions and all the node objects with the following format:
 - tth['setNodeConfig'] is a function used to save data to your node, useful for more complex nodes where the DM can tweak settings
 - tth['setContentValidator'] is a function used to set a validator for the node settings.
 - your system.js file should set tth['yourNodeName'] = { template: template literal, init: initializing function with no args };

The workflow is as follows
 - the DM will select 'create new system', and enter a title for it
 - for each node, system_maker.js will create a text input for the node title
 - the template literal will be loaded into the system form
 - if it exists, the init function will be ran. This can do anything from set default values to setting interactions between parts of the node form. See the node 'complex multiple choice' for an example.
 - if you use setContentValidator, that will run with each modification and enable/disable the submit button.


# character.js

On the character side, tth[] is a global that contains all the nodes in the system that was created by the DM, and all the data associated with those nodes.
 - your character.js file should set tth['yourNodeName'] = { template: template literal, init: initializing function that takes (title, config, callback)

The workflow is as follows
 - the players will select 'create new character', select the system, and enter a title for the character
 - for each node, character_maker.js will append the template literal and call the init function with (nodeTitle, nodeConfigs, nodeCallback)
 - your init function should do the following things with these arguments:
 - - display the title the DM selected for the node
 - - use the configs to modify the node per the DM's settings
 - - your node should pass a dictionary of traits to the callback, and an optional second argument of a value to display to the player. this will allow multiple nodes to affect the same stats, much how in Dungeons & Dragons, your characters traits can be affected by both the choice of class & the character race, and the optional second option allows you to simplify the view to the player, i.e. class: barbarian, instead of a dictionary of all the modifiers of that class choice.
 - note that no validation should be needed since all forms in the template literal will be required. If necessary, you can modify that in your init function

If you want to get real wild, you can dump some libraries in the static folder and modify the templates to load them so you can use them in your scripts. This is supposed to be a helper, feel free to hack it up and make it yours!
