let cmc = (function() {
	const template= (
		`<form id='cmcForm'>
			<fieldset id='inputFields'> </fieldset>
			<input type='submit' id='submitCMCForm' value='submit'></input>
		</form>`
	);

	const init = function(nodeTitle, nodeConfig, submitCallBack) {
		$('#inputFields').append(
			$('<legend></legend>').html('Select your '+nodeTitle)
		);

		let choiceTemplate = function(label, description){
			return(
				`<div>
					<input type='radio' name='${nodeTitle}' value='${label}'
						id='${label}input' required>
						<label for='${label}input'>${label}</label>
					</input>
					<span>${description}</span>
					<p>Traits for this choice</p>
					<ul id='traitsFor${label}'>
					</ul>
				</div>`
			);
		};

		for (const c of nodeConfig) {
			$('#inputFields').append(choiceTemplate(c.label, c.description));
			Object.keys(c.traits).forEach( k => {
				$('#traitsFor' + c.label).append(`<li>${k} : ${c.traits[k]}</li>`);
			});
		}
		$('#cmcForm').submit( e => {
			let choices = $(`#cmcForm input:radio[name='${nodeTitle}']`);
			let playerChoiceIndex = choices.index(choices.filter(':checked'));
			let playerChoiceValue = (choices[playerChoiceIndex]).value;
			let traitsValues = nodeConfig[playerChoiceIndex];
			console.log("CMC FORM:");
			console.log("player chose:", playerChoiceIndex);
			console.log("with value:", (choices[playerChoiceIndex]).value);
			traitsValues = (nodeConfig[playerChoiceIndex]).traits;
			traitsValues[nodeTitle] = playerChoiceValue;
			console.log("resulting in these traits:", traitsValues);
			submitCallBack(traitsValues, {[nodeTitle]:playerChoiceValue});
		});
	};

	return ({ 'template': template, 'init': init }); })();

tth['complex multiple choice'] = {'template': cmc.template, 'init': cmc.init};
