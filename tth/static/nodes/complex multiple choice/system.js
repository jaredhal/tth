let complexMultipleChoice = (function(){
	const template = (`<div id='complexMultipleChoiceNode'>
		<p>
			This will be a multiple choice node for your players. Enter the ` +
			`choice label displayed to your players, a description of the ` +
			`choice, and the traits & values to be applied to the players' ` +
			`characters.
		</p>
		<p>
			You can also add traits to be merged into the character depending ` +
			`on their choice. For example, for a node "class" with an option ` +
			`"fighter" you could give a class bonus of +1 to strength by adding ` +
			`trait:"strength" value:"1" to the option. This will work regardless `+
			`of whether "strength" is a previously appended trait or not.` +
		`</p>
		<div id='choicesContainer'></div>
		<input type='button' id='addChoice' value='add choice'></input>
		<input type='button' id='removeChoice' value='remove choice'></input>
	</div>`);

	const init = function() {
		let container = $('#choicesContainer');
		let nodeConfig = [];
		let choiceIndex = 0;
		let traitIndices = [];
		let valid = false;
		let choiceTemplate = function(index) {
			return (`<div id='choiceContainer${index}'>
				<label for='choice${index}'>Choice ${index} : </label>
				<input type='text' required='true' id='choice${index}'
					minlength='1' maxlength='100'>
				</input>
				<label for='choice${index}Desc'>Description ${index}</label>
				<input type='text' id='choice${index}Desc'></input>
				<div id='traitsContainer${index}'>
				</div>
				<input type='button' id='addTrait${index}' value='add trait'></input>
				<input type='button' id='removeTrait${index}' value='remove trait'></input>
			</div>`);
		};
		let traitTemplate = function(index, tIndex) {
			return (`<div id='choice${index}TraitContainer${tIndex}'>
				<span>&nbsp;</span>
				<label for='choice${index}Traits${tIndex}'>Choice ${index} - Trait ${tIndex}</label>
				<input type='text' id='choice${index}Trait${tIndex}'></input>
				<label for='choice${index}Value${tIndex}'>Value for Trait ${tIndex}</label>
				<input type='text' id='choice${index}Value${tIndex}'></input>
			</div>`);
		};

		function updateConfig() {
			nodeConfig = [];
			valid = true;
			for (let i = 0; i < container.children().length; i++) {
				if ($('#choice' + i).val() != '') {
					let t = {};
					let tContainer = $('#traitsContainer' + i);
					for (let j = 0; j < tContainer.children().length; j++) {
						let key = tContainer.find('input[id=choice'+i+'Trait'+j+']').val();
						let val = tContainer.find('input[id=choice'+i+'Value'+j+']').val();
						t[key] = val;
					}
					nodeConfig.push({
						'label': $('#choice'+i).val()
						,'description': $('#choice'+i+'Desc').val()
						,'traits': t
					});
				} else {
					valid = false;
				}
			}
			tth.setNodeConfig(nodeConfig);
		}

		function validateChoices() {
			return valid;
		}

		function addChoice() {
			const i = choiceIndex;
			traitIndices[i] = 0;
			container.append(choiceTemplate(choiceIndex));
			$('#addTrait' + choiceIndex).click( e => {addTrait(i)});
			$('#removeTrait' + choiceIndex).click( e => {removeTrait(i)});
			$('#removeChoice' + choiceIndex).prop('disabled', false);
			$('#choice'+choiceIndex).change(updateConfig);
			$('#choice'+choiceIndex+'Desc').change(updateConfig);
			choiceIndex += 1;
			updateConfig();
		}

		function removeChoice() {
			let choices = container.children().length;
			if (choices > 1) {
				container.children()[choices - 1].remove();
				choiceIndex -= 1;
				if (choices === 2) {
					$('#removeChoice').prop('disabled', true);
				}
			}
			updateConfig();
		}

		function addTrait(choiceIndex) {
			$('#traitsContainer'+choiceIndex).append(
				traitTemplate(choiceIndex, traitIndices[choiceIndex])
			);
			$('#choice'+choiceIndex+'Trait'+traitIndices[choiceIndex]).change(updateConfig);
			$('#choice'+choiceIndex+'Value'+traitIndices[choiceIndex]).change(updateConfig);
			$('#removeTraits'+choiceIndex).prop('disabled', false);
			traitIndices[choiceIndex] += 1;
		}

		function removeTrait(choiceIndex) {
			traitIndices[choiceIndex] -= 1;
			$('#choice'+choiceIndex+'TraitContainer'+traitIndices[choiceIndex]).remove();
			if (traitIndices[choiceIndex] <= 1) {
				$('#removeTraits' + choiceIndex).prop('disabled', true);
			}
		}

		tth.setContentValidator(validateChoices);
		$('#addChoice').click(addChoice);
		$('#removeChoice').click(removeChoice);
		addChoice();
		addChoice();
	};

	return ({
		template: template
		,init: init
	});
})();

tth['complex multiple choice'] = {
	template: complexMultipleChoice.template
	,init: complexMultipleChoice.init
};
