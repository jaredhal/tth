let text = (function (){
	const template = ` <form id='textInputNode'>
		<label id='textNodeLabel'></label>
		<input type='text' id='textNodeInput' required></input>
		<input type='submit' id='submitTextNode' value='submit node'></input>
	</form> `;

	const init = function(nodeTitle, nodeConfig, submitCallBack){
		$('#textNodeLabel').html(nodeTitle);
		$('#textInputNode').submit( e => {
			let c = { [nodeTitle] : $('#textNodeInput').val() };
			console.log('submitting', c);
			submitCallBack(c, $('#textNodeInput').val());
		});
	};

	return ({ template: template ,init: init });
}());

tth['text input'] = {
	template: text.template
	,init: text.init
};
