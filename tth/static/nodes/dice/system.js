let dice = (function(){

	const template = (`<div id='diceNode'>
			<p>This node will be a number of dice rolls your players can make.</p>
			<p>The sum total of the dice rolls will be appended to their character under the node title.</p>
			<p>You can optionally allow one mulligan (reroll)</p>
			<div id='diceContainer'></div>
			<input type='button' id='addRoll' value='add roll'></input>
			<input type='button' id='removeRoll' value='remove roll'></input>
	</div>`);

	const init = function() {
		let container = $('#diceContainer');
		let addBtn = $('#addRoll');
		let rmvBtn = $('#removeRoll');
		let rollIndex = -1;

		function validate() {
			return true;
		}

		function updateConfig() {
			let rolls = container.children();
			let nodeConfig = [];
			container.children().each( function(i, el) {
				nodeConfig.push({
					'diceNum': el.children[1].value
					,'diceVal': el.children[3].value
					,'reroll': el.children[5].checked
				});
			});
			tth.setNodeConfig(nodeConfig);
		}

		function rollTemplate(index) {
			return (`<div id='roll${index}'>
				<label for='dnum${index}'>roll ${index}</label>
				<input type='number' id='dnum${index}' value='1' min='1' required></input>
				<span>D</span>
				<input type='number' id='dval${index}' value='6' min='1' required></input>
				<label for='rerolls${index}'>Allow reroll?</label>
				<input type='checkbox' id='reroll${index}'</input>
			</div>`);
		}

		function addRoll() {
			rollIndex += 1;
			container.append(rollTemplate(rollIndex));
			if (rollIndex >= 1) {
				rmvBtn.prop('disabled', false);
			}
			container.find('input').change(updateConfig);
			updateConfig();
		}

		function removeRoll() {
			$('#diceContainer').children()[rollIndex - 1].remove();
			rollIndex -= 1;
			if (rollIndex <= 0) {
				rmvBtn.prop('disabled', true);
			}
			updateConfig();
		}

		tth.setContentValidator(validate);
		$('#addRoll').click(addRoll);
		$('#removeRoll').click(removeRoll);
		rmvBtn.prop('disabled', true);
		addRoll();
	};

	return ({
		template: template
		,init: init
	});
})();

tth['dice'] = {
	template: dice.template
	,init: dice.init
};
