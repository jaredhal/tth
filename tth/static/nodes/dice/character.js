let dice = (function(){

	const template = (`<div id='diceNodeContainer'
		<h4 id='diceTitle'></h4>
		<p>Roll for your character</p>
		<form' id='diceForm'>
			<div id='diceContainer'></div>
			<input type='submit'  id='diceFormSubmit' value='submit'></input>
		</form>
	</div>`);


	const init = function(nodeTitle, nodeConfig, submitCallBack){
		let container = $('#diceContainer');
		let index = 0;
		let results = [];

		function checkValid() {
			let submittable = true;
			for (r of results) {
				submittable = submittable && ( r != -1 );
			}
			return submittable;
		}

		function submitDice() {
			
			submitCallBack({[nodeTitle]: results}, results);
		}

		function rollTemplate(index, diceNum, diceVal, reroll) {
			let base = $(`<div id='roll${index}'>
				<span id='displayNum${index}'>${diceNum} d ${diceVal}</span>
				<input type='button' id='rollBtn${index}' value='roll'
					required></input>
				<span id='resultsNum${index}'></span>
			</div>`);
			base.data('reroll', reroll);
			base.data('diceNum', diceNum);
			base.data('diceVal', diceVal);
			base.find('input[id=rollBtn'+index+']').click( e => roll(index));
			base.find('input[id=rollBtn'+index+']');
			return base;
		}

		function roll(index) {
			let container = $('#roll'+index);
			let reroll = container.data('reroll');
			let diceNum = Number(container.data('diceNum'));
			let diceVal = Number(container.data('diceVal'));
			let result = 0;
			for (var i = 0; i < diceNum; i++ ) {
				result += Math.floor(Math.random() * Math.floor(diceVal));
			}
			container.find('span[id=resultsNum'+index+']').html(result);
			if ( reroll ) {
				container.data('reroll', false);
			} else {
				container.find('input').first().prop('disabled', true);
			}
			results[index] = result;
			$('#diceFormSubmit').prop('disabled', !(checkValid()));
		}

		for (n of nodeConfig) {
			$('#diceFormSubmit').prop('disabled', true);
			container.append(rollTemplate(index, n.diceNum, n.diceVal, n.reroll));
			index += 1;
			results.push(-1);
		}
		$('#diceTitle').html(nodeTitle);
		$('#diceFormSubmit').click(submitDice);
	}

	return ({
		template: template
		,init: init
	});
})();

tth['dice'] = {
	template: dice.template
	,init: dice.init
	
}
