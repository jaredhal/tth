let mc = (function(){

	const template = `<div>
		<p>This will be multiple choice</p>
		<div id='choicesContainer'>
		</div>
		<input type='button' id='addChoice' value='add choice' />
		<input type='button' id='removeChoice' value='remove choice' />
	</div>`
	;

	const init = function(){
		let container = $('#choicesContainer');
		let choiceIndex = -1;
		let choiceTemplate = function(index) {
			return `<div id='choiceContainer${index}'>
					<label for='choice${index}'>Choice ${index} : </label>
					<input type='text' required='true' id='choice${index}' 
						minlength='1' maxlength='100' />
				</div>`;
		};
		let valid = false;

		function updateConfig() {
			let choices = $('#choicesContainer').find("input");
			nodeConfig = [];
			valid = true;
			for (let c of choices) {
				if (c.value != ""){
					nodeConfig.push(c.value);
				} else {
					valid = false;
				}
			}
			//tth.setNodeConfig defined in system_maker.js
			tth.setNodeConfig(nodeConfig);
		}

		function validateChoices() {
			return valid;
		}

		function addChoice() {
			choiceIndex += 1;
			let n = choiceTemplate(choiceIndex);
			container.append(n);
			container.find('input[id=choice' + choiceIndex + ']').change(updateConfig);
			$('#removeChoice').prop('disabled', false);
			updateConfig();
		}

		function removeChoice() {
			let choices = container.children().length;
			if (choices > 1) {
				container.children()[choices - 1].remove();
				choiceIndex -= 1;
				if (choices === 2) {
					$('#removeChoice').prop('disabled', true);
				}
			}
			updateConfig();
		}

		tth.setContentValidator(validateChoices);
		$('#addChoice').click(addChoice);
		$('#removeChoice').click(removeChoice);
		addChoice();
		addChoice();
	};

	return ({
			template: template
			,init: init
		});

}());

tth['multiple choice'] = {
	template: mc.template
	,init: mc.init
};
