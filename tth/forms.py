from flask_wtf import FlaskForm
from wtforms import StringField, RadioField, HiddenField, PasswordField, SubmitField
from wtforms.validators import DataRequired, EqualTo, Length

from tth.nodes import get_nodes

class Login_Form(FlaskForm):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    submit = SubmitField('login')

class Register_Form(FlaskForm):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    confirm_password = PasswordField('confirm password'
        ,validators=[DataRequired(), EqualTo('password')]
    )
    submit = SubmitField('register')

class System_Form(FlaskForm):
    sys_title = StringField('Title', validators=[DataRequired()])
    node_list = HiddenField('node_list', validators=[DataRequired()])
    sys_submit = SubmitField('finish')

class Node_Form(FlaskForm):
    node_title = StringField('node title', validators=[DataRequired()])
    node_list = []
    for n in get_nodes():
        node_list.append((n,n))
    node_type = RadioField('node type', choices=node_list
        ,default=str(node_list[0][0]), validators=[DataRequired()])
    node_config = HiddenField('node config')
    node_submit = SubmitField('submit node')

class Pick_Sys_Form(FlaskForm):
    systems = RadioField('system', choices=list(), coerce=int, validators=[DataRequired()])
    submit = SubmitField('submit')

class Character_Form(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    traits = HiddenField('traits', validators=[DataRequired()])
    submit = SubmitField('Finish Character')
