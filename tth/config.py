class Config(object):
    DEBUG = True
    DEVELOPMENT = True
    FLASK_ENV = 'development'
    SECRET_KEY = 'wow I should really change this'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
