import json

from flask import Blueprint, render_template, session, redirect, url_for, request, g, flash

from .db import db
from .auth import login_required
from .nodes import get_nodes
from .forms import System_Form, Node_Form
from .models import System

bp = Blueprint('systems', __name__, url_prefix='/systems')

def validate(title, nodes):
    valid = (title is not None and type(title) is str)
    for n in nodes:
        valid = (valid and type(n['type']) is str)
        try:
            n['config']
        except IndexError:
            valid = false
    return valid

@bp.route('/list', methods=['GET'])
@login_required
def list():
    return render_template('systems/list.html', systems=System.query.all())

@bp.route('/create', methods=['GET', 'POST'])
@login_required
def create():
    system_form = System_Form()
    node_form = Node_Form()
    node_list = get_nodes()
    if system_form.validate_on_submit():
        title = request.form['sys_title']
        nodes = json.loads(request.form['node_list'])
        if not validate(title, nodes):
            flash('invalid system')
        else:
            n = json.dumps(nodes)
            s = System(title=title, nodes=n, owner_id=session.get('id'))
            db.session.add(s)
            db.session.commit()
            return redirect(url_for('systems.list'))
    return render_template('systems/create.html'
        ,system_form=system_form
        ,node_form=node_form,
        node_list=node_list
    )

@bp.route('/delete/<int:system_id>/', methods=['GET'])
@login_required
def delete(system_id):
    sys = System.query.filter_by(id=system_id).first()
    if sys is not None and sys.owner_id == session.get('id'):
        db.session.delete(sys)
        db.session.commit()
    else:
        flash('users can only delete their own systems')
    return redirect(url_for('systems.list'))
