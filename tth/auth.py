import functools
from flask import Blueprint, g, redirect, url_for, session

bp = Blueprint('auth', __name__, url_prefix='/auth')

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('users.login'))
        return view(**kwargs)
    return wrapped_view

@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('id')
    if user_id is None:
        g.user = None
    else:
        from tth.models import User
        g.user = User.query.filter_by(id=user_id).first()
