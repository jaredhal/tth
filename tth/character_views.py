import json
from flask import Blueprint, session, request, render_template, redirect, flash, url_for, g

from tth.auth import login_required
from tth.nodes import get_nodes
from tth.db import db
from .forms import Character_Form, Pick_Sys_Form
from .models import System, Character

bp = Blueprint('characters', __name__, url_prefix='/characters')

@bp.route('/list', methods=['GET'])
@login_required
def list():
    c_list = Character.query.filter_by(owner_id=session['id']).all()
    return render_template('characters/list.html' ,character_list=c_list)

@bp.route('/view/<int:character_id>/', methods=['GET'])
@login_required
def view_char(character_id):
    c = Character.query.filter_by(id=character_id).first()
    if c is None or c.owner_id != session.get('id'):
        flash('you can only view your own characters')
        return redirect(url_for('characters.list'))
    char_traits = json.loads(json.loads(c.traits))
    return render_template('characters/view.html'
        ,char=c
        ,system=c.system.title
        ,traits=char_traits
    )

@bp.route('/create/pick_sys', methods=['GET', 'POST'])
@login_required
def pick_sys():
    form = Pick_Sys_Form()
    if System.query.count() == 0:
        systems_exist = False
    else:
        for s in System.query.all():
            form.systems.choices.append((s.id, s.title))
        systems_exist = True

    if form.validate_on_submit():
        system_id = request.form['systems']
        return redirect(url_for('characters.create', system_id=system_id))

    return render_template('characters/pick_sys.html', form=form, systems_exist=systems_exist)

@bp.route('/create/<int:system_id>/', methods=['GET', 'POST'])
@login_required
def create(system_id):
    form = Character_Form()
    if form.validate_on_submit():
        name = request.form['name']
        traits = json.dumps(json.loads(request.form['traits']))
        c = Character(name=name, traits=json.dumps(traits), owner_id=session.get('id'), system_id=system_id)
        db.session.add(c)
        db.session.commit()
        return redirect(url_for('characters.list'))
    else:
        if request.method == 'POST':
            flash(form.errors)
        system = System.query.filter_by(id=system_id).first()
        nodeList = json.loads(system.nodes)
        nodeTypes = get_nodes()
    return render_template('characters/create.html'
        ,form=form ,title=system.title ,nodeList=nodeList ,nodeTypes=nodeTypes)

@bp.route('/delete/<int:character_id>/', methods=['GET'])
@login_required
def delete(character_id):
    c = Character.query.filter_by(id=character_id).first()
    if c is not None and c.owner_id == session.get('id'):
        db.session.delete(c)
        db.session.commit()
    else:
        flash('users can only delete their own characters')
    return redirect(url_for('characters.list'))
