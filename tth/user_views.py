from flask import (
    Blueprint, request, url_for, render_template, redirect, flash, session, g
)
from werkzeug.security import check_password_hash, generate_password_hash

from .auth import login_required
from .forms import Register_Form, Login_Form
from .db import db
from .models import User

bp = Blueprint('users', __name__, template_folder='templates')

@bp.route('/register', methods=['GET', 'POST'])
def register():
    form = Register_Form()
    username = None
    error = None
    if form.validate_on_submit():
        username = request.form['username']
        password = request.form['password']
        if User.query.filter_by(username=username).first() is not None:
            error = 'username already exists'
            flash(error)
            return redirect(url_for('users.login'))
        else:
            db.session.add(User(username=username, password=generate_password_hash(password)))
            db.session.commit()
            return redirect(url_for('users.login'))
    return render_template('register.html', form=form)

@bp.route('/login', methods=['GET', 'POST'])
def login():
    form = Login_Form()
    username = None
    error = None
    if g.user is not None:
        return redirect(url_for('home'))
    elif form.validate_on_submit():
        username = request.form['username']
        password = request.form['password']
        u = User.query.filter_by(username=username).first()
        if u is None:
            error = 'user does not exist'
        elif not check_password_hash(u.password, password):
            error = 'incorrect password'
        if error is None:
            session.clear()
            session['id'] = u.id
            return redirect(url_for('home'))
        else:
            flash(error)
    return render_template('login.html', form=form)

@bp.route('/logout', methods=['GET'])
@login_required
def logout():
    session.clear()
    flash('logged out')
    return redirect(url_for('home'))
