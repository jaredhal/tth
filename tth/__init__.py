import os
from flask import Flask, session, redirect, url_for
from flask_sqlalchemy import SQLAlchemy

def create_app(config=None):
    app = Flask(__name__, instance_relative_config=True)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    if config is None:
        from .config import Config
        app.config.from_object(Config())
        print(' * loaded default testing config')
        print(" * if you're not actively developing, you should change this")
    else:
        app.config.update(config)

    with app.app_context():
        if app.config.get('SQLALCHEMY_DATABASE_URI') is None:
            app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(app.instance_path, 'tth.db')
            app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        from .db import db
        from .models import User, System, Character
        db.init_app(app)

        @app.cli.command('init-db')
        def init_db_command():
            db.create_all()
            db.session.commit()
            print('database initialized at', app.config['SQLALCHEMY_DATABASE_URI'])

        from tth.auth import bp as auth_bp
        from tth.user_views import bp as user_bp
        from tth.system_views import bp as sys_bp
        from tth.character_views import bp as char_bp
        app.register_blueprint(auth_bp)
        app.register_blueprint(user_bp)
        app.register_blueprint(sys_bp)
        app.register_blueprint(char_bp)

        @app.route('/')
        def home():
            return redirect(url_for('systems.list'))

    return app
